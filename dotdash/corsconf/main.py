

#CORS_URLS_REGEX = r'^/.*$' # CORS HEADERS ENABLED
CORS_URLS_REGEX = r'^/api/.*$' # CORS HEADERS ENBALED

CORS_ORIGIN_WHITELIST = (
    'https://goodreads.square400.com',
    'https://www.goodreads.square400.com'
)


from corsheaders.defaults import default_headers

CORS_ALLOW_HEADERS = default_headers + (
    'X-CSRFToken',
)