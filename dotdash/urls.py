from django.contrib import admin
from django.urls    import path
from django.conf.urls import url, include
import alpha.views



urlpatterns = [
    path('admin/', admin.site.urls),
    url(r'^api/books/(?P<q>.*)/(?P<page>\d+)/$',     alpha.views.api),
    url(r'^$',                                       alpha.views.homepage),

    

]
