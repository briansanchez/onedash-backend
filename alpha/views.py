import requests, xmltodict, json
from django.http        import JsonResponse
from django.shortcuts   import  render
from django.conf        import settings

def homepage(request):
    remote_addr = request.META.get('HTTP_X_REAL_IP', request.META.get('REMOTE_ADDR', None))
    context     = {'remote_addr': remote_addr}
    return render(request, 'index.html', context) 

""" 
  Basic endpoint to connect to the Goodreads search API, the API returns XML; the XML is transformed into JSON, to return basic books info
"""
def api(request, q, page = 1):
    api_url       = 'https://www.goodreads.com/search/index.xml?key=' + settings.GOODREADS_KEY + '&q=' + q + '&page=' + str(page)
    response      = requests.get(api_url) #, headers = data)
    
    if response.status_code == 200:
        jsonn     = json.loads(json.dumps(xmltodict.parse( response.content ) ) )
        jsonn     = jsonn['GoodreadsResponse']['search']
    else:
        jsonn     = {'status_code': response.status_code, 'error': True}
    return JsonResponse(jsonn)
